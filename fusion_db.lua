reactor = null
reactors = {}
json = require "json"
rednet.open("back")
previouspost = ""
serverurl = "http://192.168.1.100:8123"
servertoken = "averysecurtokennoonewilleverguessthisforsureibetyouliketendollars"

function initPeripherals()
    local perList = peripheral.getNames()

	reactorNumber = 1
	monitorFound = false
    for i=1,#perList,1 do
        if peripheral.getType(perList[i]) == "Reactor Logic Adapter" then
            table.insert(reactors, reactorNumber, perList[i])
        end
    end

	reactor = peripheral.wrap(reactors[1])
end

function webdisplay()
	while true do
		urlencoded = ""
		energy_output_RF = ( reactor.getProducing() / 2.5)
		if energy_output_RF > 1000000 then
			energy_output_formatted = string.format("%.2f",(energy_output_RF / 1000000)) .. " MRF/t"
		elseif energy_output_RF > 1000 then
			energy_output_formatted = string.format("%.2f",(energy_output_RF / 1000)) .. " KRF/t"
		else
			energy_output_formatted = string.format("%.2f",energy_output_RF) .. " RF/t"
		end
		
		plasma_temp_raw = reactor.getPlasmaHeat()
		plasma_temp_formatted = math.floor((plasma_temp_raw / 1000000))
		
		
		if reactor.isIgnited() then 
			rs = 1
		else
			rs = 0
		end
		
		reactortable = {
		reactor_state = rs,
		energy_output = math.floor((reactor.getProducing() / 2.5)),
		injection_rate = reactor.getInjectionRate(),
		plasma_temp = math.floor(reactor.getPlasmaHeat()),
		token = servertoken
		}
		
		
		
		for command,value in pairs(reactortable) do
			urlencoded = urlencoded .. "&" .. command .. "=" .. value
		end
		
		--output = fs.open("wwwroot/interface.json","w")
		
		http.post(serverurl.. "/interface", urlencoded)

		sleep(1)
	end
end


function webcontrol ()
	while true do
		commandresult = ""
		 response = http.get(serverurl .. "/pullcommand",{Authorization = servertoken})
		 if response then
		 response_string = response.readAll()
			if response_string ~= "" then	
				commands = json.decode(response_string)
					for command,value in pairs(commands) do
						if command == "setinject" then
							reactor.setInjectionRate(tonumber(value))
							commandresult = "Injection rate set to " .. value
						elseif command == "startreactor" then
							rednet.send(6,"loadhohlraum","command")
							local sender,message,protocol = rednet.receive(any,120)
							if protocol == "success" then
								if (reactor.getInjectionRate() < 2) then
									reactor.setInjectionRate(2)
								end
								redstone.setOutput("right",true)
								sleep(1)
								redstone.setOutput("right",false)
							else commandresult = "message"
							end
						end
					end
			end
		end
		sleep(1)
	end

end

initPeripherals()

parallel.waitForAny(webdisplay,webcontrol)

