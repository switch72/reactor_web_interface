const LocalStrategy = require('passport-local').Strategy

const ygg = require('yggdrasil')({
  host: 'https://authserver.mojang.com'
});


function initialize(passport, getUserByName, getUserById) {
  const authenticateUser = async (email, password, done) => {
    ygg.auth({
	  token: '', //Optional. Client token.
	  agent: '', //Agent name. Defaults to 'Minecraft'
	  version: 1, //Agent version. Defaults to 1
	  user: email, //Username
	  pass: password //Password
	}, function(err, data){ 
			if (err) {
				console.log(err)
				return done(null, false,  {message: "Login Failed"})
			}else {
				user = getUserById(data.selectedProfile.id)
				if (user == null){
					return done(null, false,  {message:  'User not authorized for control'})
				}
				return done(null, data.selectedProfile)
			}
		});
  }

  passport.use(new LocalStrategy({ usernameField: 'email' }, authenticateUser))
  passport.serializeUser((user, done) => done(null, user.id))
  passport.deserializeUser((id, done) => {
    return done(null, getUserById(id))
  })
}

module.exports = initialize